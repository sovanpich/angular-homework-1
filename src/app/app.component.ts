import { Component } from '@angular/core';
import {style} from "@angular/animations";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  text! : string;
  imgOff = "./assets/img/off.png";
  imgOn="./assets/img/on.png";
  click: boolean = true;
  color!:string;
   onClick(){
       this.click = ! this.click;
   }
}
